package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class UserPage {
    private WebDriver driver;
    private By statusAuthentication = By.xpath("//span[contains(@class,'jsx-2454436330')] [contains(text(),'Magdalena')]");

    public UserPage(WebDriver driver){
        this.driver = driver;
    }

    public String getAutheticationStatus(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver.findElement(statusAuthentication).getText();
    }
}
