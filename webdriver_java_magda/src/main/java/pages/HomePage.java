package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class HomePage {
    private WebDriver driver;

    private By myAccount = By.xpath("//*[@id=\"__next\"]/div[2]/div[1]/div/div/div[2]/div[3]/div[1]/a/div[3]/span");
    private By searchField = By.xpath("//*[@id=\"__next\"]/div[2]/div[1]/div/div/div[2]/div[4]/form/div/div[1]/input");
    private By searchButton = By.xpath("//*[@id=\"__next\"]/div[2]/div[1]/div/div/div[2]/div[4]/form/div/div[2]/button/div/div/div");
    private By category = By.linkText("Laptop, Desktop, IT, Birotica");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public LoginPage clickMyAccount(){
    driver.findElement(myAccount).click();
    return new LoginPage(driver);
    }

    public void setSearchField(String searchByWord){
        driver.findElement(searchField).sendKeys(searchByWord);
    }

    public SearchPage clickSearchButton(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(searchButton).click();
        return new SearchPage(driver);
    }

    public CategoryPage clickCategory(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(category).click();
        return new CategoryPage(driver);
    }
}
