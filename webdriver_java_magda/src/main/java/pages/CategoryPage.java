package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CategoryPage {
    private WebDriver driver;
    private By selectCategoryStatus = By.xpath("//*[@id=\"__next\"]/div[2]/div[2]/div[2]/ul/li[1]/ul/li[2]/div/div/ul/li[1]/a");

    public CategoryPage(WebDriver driver){
        this.driver = driver;
    }

    public String getSelectCategoryStatus(){
        return driver.findElement(selectCategoryStatus).getText();
    }


}
