package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class SearchPage {
    private WebDriver driver;
    private By searchStatus = By.xpath("//*[@id=\"__next\"]/div[3]/main/div[2]/div[1]/div[1]/h1");

    public SearchPage(WebDriver driver){
        this.driver = driver;
    }

    public String getSearchStatus(){
        return driver.findElement(searchStatus).getText();
    }
}
