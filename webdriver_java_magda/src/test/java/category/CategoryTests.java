package category;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.CategoryPage;

import static org.testng.Assert.assertEquals;

public class CategoryTests extends BaseTests {
    @Test
    public void testSuccessfulSelectCategory(){
        homePage.clickCategory();
        assertEquals(categoryPage.getSelectCategoryStatus(), "Laptopuri", "Search failed");
    }
}
