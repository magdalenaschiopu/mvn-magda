package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.HomePage;
import pages.CategoryPage;
import pages.SearchPage;

public class BaseTests {
    private WebDriver driver;
    protected HomePage homePage;
    protected SearchPage searchPage;
    protected CategoryPage categoryPage;


@BeforeClass
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:/Users/maschiopu/Workspace/webdriver_java_magda/resources/chromedriver.exe");
                driver = new ChromeDriver();
        driver.get("https://altex.ro/");

        homePage = new HomePage(driver);
        searchPage = new SearchPage(driver);
        categoryPage = new CategoryPage(driver);

        driver.manage().window().maximize();
        System.out.println(driver.getTitle());

    }

    @AfterClass
    public void tearDown(){
    //driver.quit();
    }

}
