package search;

import base.BaseTests;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SearchPage;

import static org.testng.Assert.assertEquals;

public class SearchTests extends BaseTests {
    @Test
    public void testSuccessfulSearch(){
      homePage.setSearchField("smart");
      SearchPage searchPage = homePage.clickSearchButton();
      assertEquals(searchPage.getSearchStatus(), "Rezultate cautare : smart", "Search failed");
    }
}
